﻿using System;
using System.Collections;
using System.Linq;
using HMAFlex.Controllers;

namespace HMAFlex.Models;

public class CheckOut
{
    private static string tableName;
    
    private int id; //checkinout log id
    private CheckInOutType type;
    private int sid; //student id
    private DateTime date;
    private DateTime time; //DateTime.Now.ToString("HH:mm:ss");

    public CheckOut(int id, CheckInOutType type, int sid, DateTime date, DateTime time)
    {
        tableName = "CheckInOut";
        
        this.id = id;
        this.type = type;
        this.sid = sid;
        this.date = date;
        this.time = time;

        if (this.id == 0)
        {
            ArrayList infoString = new ArrayList();
            infoString.Add(Util.CheckInOutTypeToString(type));
            infoString.Add(sid.ToString());
            infoString.Add(date.ToShortDateString());
            infoString.Add(time.ToString("HH:mm:ss"));
            infoString.Add("Out");
        
            Program.DB.Insert(tableName,
                "Type, SID, Date, Time, InOut",
                string.Join(", ", infoString));
        }
    }

    public CheckOut()
    {
        tableName = "CheckInOut";

        id = 0;
        type = CheckInOutType.None;
        sid = 0;
        date = DateTime.Now;
        time = DateTime.Now;
        
        ArrayList infoString = new ArrayList();
        infoString.Add(Util.CheckInOutTypeToString(type));
        infoString.Add(sid.ToString());
        infoString.Add(date.ToShortDateString());
        infoString.Add(time.ToString("HH:mm:ss"));
        infoString.Add("Out");

        Program.DB.Insert(tableName,
            "Type, SID, Date, Time, InOut",
            string.Join(", ", infoString));
    }

    public int Id => id;

    public CheckInOutType Type
    {
        get => type;
        set
        {
            type = value;
            Program.DB.Modify(id, tableName, "Type", Util.CheckInOutTypeToString(type));
        }
    }

    public int SID
    {
        get => sid;
        set
        {
            sid = value;
            Program.DB.Modify(id, tableName, "SID", sid.ToString());
        }
    }

    public DateTime Date
    {
        get => date;
        set
        {
            date = value;
            Program.DB.Modify(id, tableName, "Date", date.ToShortDateString());
        }
    }

    public DateTime Time
    {
        get => time;
        set
        {
            time = value;
            Program.DB.Modify(id, tableName, "Time", time.ToString("HH:mm:ss"));
        }
    }
}