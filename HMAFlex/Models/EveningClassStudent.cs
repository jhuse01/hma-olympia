﻿using System;
using System.Collections;
using System.Linq;
using System.Security.AccessControl;
using HMAFlex.Controllers;

namespace HMAFlex.Models;

public class EveningClassStudent
{
    private static string tableName;
    
    private int id;
    private string firstName;
    private string lastName;
    private DateTime birthday;
    private string email;
    private string address;

    private string courseType;
    private DateTime membershipExpiration;
    private DateTime joinDate;
    private TaekwondoRank taekwondoRank;
    private JudoRank judoRank;
    private bool currentStudent;
    private EveningClassProgram status;

    private string contactOne;
    private string phoneOne;
    private string relationOne;
    
    private string contactTwo;
    private string phoneTwo;
    private string relationTwo;
    
    private string contactThree;
    private string phoneThree;
    private string relationThree;

    public EveningClassStudent(int id, string firstName, string lastName, string courseType, TaekwondoRank taekwondoRank, JudoRank judoRank, EveningClassProgram status, bool currentStudent, DateTime membershipExpiration, DateTime birthday, string email, string address, string contactOne, string relationOne, string phoneOne, string contactTwo, string relationTwo, string phoneTwo, string contactThree, string relationThree, string phoneThree, DateTime joinDate)
    {
        tableName = "EveningClassStudents";
        
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.email = email;
        this.address = address;
        this.courseType = courseType;
        this.membershipExpiration = membershipExpiration;
        this.joinDate = joinDate;
        this.taekwondoRank = taekwondoRank;
        this.judoRank = judoRank;
        this.currentStudent = currentStudent;
        this.status = status;
        this.contactOne = contactOne;
        this.phoneOne = phoneOne;
        this.relationOne = relationOne;
        this.contactTwo = contactTwo;
        this.phoneTwo = phoneTwo;
        this.relationTwo = relationTwo;
        this.contactThree = contactThree;
        this.phoneThree = phoneThree;
        this.relationThree = relationThree;

        if (this.id == 0)
        {
            ArrayList infoString = new ArrayList();
            infoString.Add(firstName);
            infoString.Add(lastName);
            infoString.Add(birthday.ToShortDateString());
            infoString.Add(email);
            infoString.Add(address);
            infoString.Add(courseType);
            infoString.Add(membershipExpiration.ToShortDateString());
            infoString.Add(joinDate.ToShortDateString());
            infoString.Add(Util.GetRankString(taekwondoRank));
            infoString.Add(Util.GetRankString(judoRank));
            infoString.Add(currentStudent.ToString());
            infoString.Add(Util.GetEveningProgramString(status));
            infoString.Add(contactOne);
            infoString.Add(phoneOne);
            infoString.Add(relationOne);
            infoString.Add(contactTwo);
            infoString.Add(phoneTwo);
            infoString.Add(relationTwo);
            infoString.Add(contactThree);
            infoString.Add(phoneThree);
            infoString.Add(relationThree);

            Program.DB.Insert(tableName,
                "FirstName, LastName, Birthday, Email, Address, CourseType, MembershipExpiration, JoinDate, TaekwondoRank, JudoRank, CurrentStudent, Status, ContactOne, PhoneOne, RelationOne, ContactTwo, PhoneTwo, RelationTwo, ContactThree, PhoneThree, RelationThree",
                string.Join(", ", infoString));
        }
    }

    public EveningClassStudent()
    {
        tableName = "EveningClassStudents";

        id = 0;
        firstName = "";
        lastName = "";
        birthday = DateTime.Now;
        email = "";
        address = "";
        courseType = "";
        membershipExpiration = DateTime.Now;
        joinDate = DateTime.Now;
        taekwondoRank = TaekwondoRank.None;
        judoRank = JudoRank.None;
        currentStudent = false;
        status = EveningClassProgram.Other;
        contactOne = "";
        phoneOne = "";
        relationOne = "";
        contactTwo = "";
        phoneTwo = "";
        relationTwo = "";
        contactThree = "";
        phoneThree = "";
        relationThree = "";
        
        ArrayList infoString = new ArrayList();
        infoString.Add(firstName);
        infoString.Add(lastName);
        infoString.Add(birthday.ToShortDateString());
        infoString.Add(email);
        infoString.Add(address);
        infoString.Add(courseType);
        infoString.Add(membershipExpiration.ToShortDateString());
        infoString.Add(joinDate.ToShortDateString());
        infoString.Add(Util.GetRankString(taekwondoRank));
        infoString.Add(Util.GetRankString(judoRank));
        infoString.Add(currentStudent.ToString());
        infoString.Add(Util.GetEveningProgramString(status));
        infoString.Add(contactOne);
        infoString.Add(phoneOne);
        infoString.Add(relationOne);
        infoString.Add(contactTwo);
        infoString.Add(phoneTwo);
        infoString.Add(relationTwo);
        infoString.Add(contactThree);
        infoString.Add(phoneThree);
        infoString.Add(relationThree);

        Program.DB.Insert(tableName,
            "FirstName, LastName, Birthday, Email, Address, CourseType, MembershipExpiration, JoinDate, TaekwondoRank, JudoRank, CurrentStudent, Status, ContactOne, PhoneOne, RelationOne, ContactTwo, PhoneTwo, RelationTwo, ContactThree, PhoneThree, RelationThree",
            string.Join(", ", infoString));
    }

    public string FirstName
    {
        get => firstName;
        set
        {
            firstName = value;
            Program.DB.Modify(id, tableName, "FirstName", firstName);
        }
    }

    public int Id => id;

    public DateTime JoinDate => joinDate;

    public string LastName
    {
        get => lastName;
        set
        {
            lastName = value;
            Program.DB.Modify(id, tableName, "LastName",lastName);
        }
    }

    public DateTime Birthday
    {
        get => birthday;
        set
        {
            birthday = value;
            Program.DB.Modify(id, tableName, "Birthday", birthday.ToShortDateString());
        }
    }

    public string Email
    {
        get => email;
        set
        {
            email = value;
            Program.DB.Modify(id, tableName, "Email", email);
        }
    }

    public string Address
    {
        get => address;
        set
        {
            address = value;
            Program.DB.Modify(id, tableName, "Address", address);
        }
    }

    public string CourseType
    {
        get => courseType;
        set
        {
            courseType = value;
            Program.DB.Modify(id, tableName, "CourseType", courseType);
        }
    }

    public DateTime MembershipExpiration
    {
        get => membershipExpiration;
        set
        {
            membershipExpiration = value;
            Program.DB.Modify(id, tableName, "MembershipExpiration", membershipExpiration.ToShortDateString());
        }
    }

    public TaekwondoRank TaekwondoRank
    {
        get => taekwondoRank;
        set
        {
            taekwondoRank = value;
            Program.DB.Modify(id, tableName, "TaekwondoStudent", Util.GetRankString(taekwondoRank));
        }
    }

    public JudoRank JudoRank
    {
        get => judoRank;
        set
        {
            judoRank = value;
            Program.DB.Modify(id, tableName, "JudoRank", Util.GetRankString(judoRank));
        }
    }

    public bool CurrentStudent
    {
        get => currentStudent;
        set
        {
            currentStudent = value;
            Program.DB.Modify(id, tableName, "CurrentStudent", currentStudent.ToString());
        }
    }

    public EveningClassProgram Status
    {
        get => status;
        set
        {
            status = value;
            Program.DB.Modify(id, tableName, "Status", Util.GetEveningProgramString(status));
        }
    }

    public string ContactOne
    {
        get => contactOne;
        set
        {
            contactOne = value;
            Program.DB.Modify(id, tableName, "ContactOne", contactOne);
        }
    }

    public string PhoneOne
    {
        get => phoneOne;
        set
        {
            phoneOne = value;
            Program.DB.Modify(id, tableName, "PhoneOne", phoneOne);
        }
    }

    public string RelationOne
    {
        get => relationOne;
        set
        {
            relationOne = value;
            Program.DB.Modify(id, tableName, "RelationOne", relationOne);
        }
    }

    public string ContactTwo
    {
        get => contactTwo;
        set
        {
            contactTwo = value;
            Program.DB.Modify(id, tableName, "contactTwo", contactTwo);
        }
    }

    public string PhoneTwo
    {
        get => phoneTwo;
        set
        {
            phoneTwo = value;
            Program.DB.Modify(id, tableName, "PhoneTwo", phoneTwo);
        }
    }

    public string RelationTwo
    {
        get => relationTwo;
        set
        {
            relationTwo = value;
            Program.DB.Modify(id, tableName, "RelationTwo", relationTwo);
        }
    }

    public string ContactThree
    {
        get => contactThree;
        set
        {
            contactThree = value;
            Program.DB.Modify(id, tableName, "ContactThree", contactThree);
        }
    }

    public string PhoneThree
    {
        get => phoneThree;
        set
        {
            phoneThree = value;
            Program.DB.Modify(id, tableName, "PhoneThree", phoneThree);
        }
    }

    public string RelationThree
    {
        get => relationThree;
        set
        {
            relationThree = value;
            Program.DB.Modify(id, tableName, "RelationThree", relationThree);
        }
    }
    

}