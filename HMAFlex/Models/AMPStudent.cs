﻿using System;
using System.Collections;
using System.Linq;
using HMAFlex.Controllers;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;

namespace HMAFlex.Models;

public class AMPStudent
{

    private static string tableName;

    public AMPStudent(int id, string firstName, string lastName, TaekwondoRank taekwondoRank, JudoRank judoRank,
        string courseType, string school, DateTime birthday, string address, string pickupOne, string relationOne,
        string cellOne, string pickupTwo, string relationTwo, string cellTwo, string pickupThree, string relationThree,
        string cellThree, string pickupFour, string relationFour, string cellFour, string physicianName,
        string physicianNumber, string physicianExt, string hospitalPref, string hospitalPhone, string hospitalExt,
        string healthConditions, string medication, string allergies, bool currentStudent, DateTime joinDate,
        DateTime membershipExpiration)
    {
        tableName = "AMPStudents";

        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.taekwondoRank = taekwondoRank;
        this.judoRank = judoRank;
        this.courseType = courseType;
        this.currentStudent = currentStudent;
        this.joinDate = joinDate;
        this.school = school;
        this.birthday = birthday;
        this.address = address;
        this.pickupOne = pickupOne;
        this.relationOne = relationOne;
        this.cellOne = cellOne;
        this.pickupTwo = pickupTwo;
        this.relationTwo = relationTwo;
        this.cellTwo = cellTwo;
        this.pickupThree = pickupThree;
        this.relationThree = relationThree;
        this.cellThree = cellThree;
        this.pickupFour = pickupFour;
        this.relationFour = relationFour;
        this.cellFour = cellFour;
        this.physicianName = physicianName;
        this.physicianNumber = physicianNumber;
        this.physicianExt = physicianExt;
        this.hospitalPref = hospitalPref;
        this.hospitalPhone = hospitalPhone;
        this.hospitalExt = hospitalExt;
        this.healthConditions = healthConditions;
        this.medication = medication;
        this.allergies = allergies;
        this.membershipExpiration = membershipExpiration;

        if (this.id == 0)
        {
            ArrayList infoString = new ArrayList();
            infoString.Add(firstName);
            infoString.Add(lastName);
            infoString.Add(Util.GetRankString(taekwondoRank));
            infoString.Add(Util.GetRankString(judoRank));
            infoString.Add(courseType);
            infoString.Add(currentStudent.ToString());
            infoString.Add(joinDate.ToShortDateString());
            infoString.Add(school);
            infoString.Add(birthday.ToShortDateString());
            infoString.Add(address);
            infoString.Add(pickupOne);
            infoString.Add(relationOne);
            infoString.Add(cellOne);
            infoString.Add(pickupTwo);
            infoString.Add(relationTwo);
            infoString.Add(cellTwo);
            infoString.Add(pickupThree);
            infoString.Add(relationThree);
            infoString.Add(cellThree);
            infoString.Add(pickupFour);
            infoString.Add(relationFour);
            infoString.Add(cellFour);
            infoString.Add(physicianName);
            infoString.Add(physicianNumber);
            infoString.Add(physicianExt);
            infoString.Add(hospitalPref);
            infoString.Add(hospitalPhone);
            infoString.Add(hospitalExt);
            infoString.Add(healthConditions);
            infoString.Add(medication);
            infoString.Add(allergies);
            infoString.Add(membershipExpiration);

            Program.DB.Insert(tableName,
                "FirstName, LastName, TaekwondoRank, JudoRank, CourseType, CurrentStudent, JoinDate, School, Birthday, Address, PickupOne, RelationOne, CellOne, PickupTwo, RelationTwo, CellTwo, PickupThree, RelationThree, CellThree, PickupFour, RelationFour, CellFour, PhysicianName, PhysicianNumber, PhysicianExt, HospitalPref, HospitalPhone, HospitalExt, HealthConditions, Medication, Allergies, MembershipExpiration",
                string.Join(", ", infoString));
        }
    }

    public AMPStudent()
    {
        tableName = "AMPStudents";

        id = 0;
        firstName = "";
        lastName = "";
        taekwondoRank = TaekwondoRank.None;
        judoRank = JudoRank.None;
        courseType = "";
        currentStudent = false;
        joinDate = DateTime.Now;
        school = "";
        birthday = DateTime.Now;
        address = "";
        pickupOne = "";
        relationOne = "";
        cellOne = "";
        pickupTwo = "";
        relationTwo = "";
        cellTwo = "";
        pickupThree = "";
        relationThree = "";
        cellThree = "";
        pickupFour = "";
        relationFour = "";
        cellFour = "";
        physicianName = "";
        physicianNumber = "";
        physicianExt = "";
        hospitalPref = "";
        hospitalPhone = "";
        hospitalExt = "";
        healthConditions = "";
        medication = "";
        allergies = "";
        membershipExpiration = DateTime.Now;

        ArrayList infoString = new ArrayList();
        infoString.Add(firstName);
        infoString.Add(lastName);
        infoString.Add(Util.GetRankString(taekwondoRank));
        infoString.Add(Util.GetRankString(judoRank));
        infoString.Add(courseType);
        infoString.Add(currentStudent.ToString());
        infoString.Add(joinDate.ToShortDateString());
        infoString.Add(school);
        infoString.Add(birthday.ToShortDateString());
        infoString.Add(address);
        infoString.Add(pickupOne);
        infoString.Add(relationOne);
        infoString.Add(cellOne);
        infoString.Add(pickupTwo);
        infoString.Add(relationTwo);
        infoString.Add(cellTwo);
        infoString.Add(pickupThree);
        infoString.Add(relationThree);
        infoString.Add(cellThree);
        infoString.Add(pickupFour);
        infoString.Add(relationFour);
        infoString.Add(cellFour);
        infoString.Add(physicianName);
        infoString.Add(physicianNumber);
        infoString.Add(physicianExt);
        infoString.Add(hospitalPref);
        infoString.Add(hospitalPhone);
        infoString.Add(hospitalExt);
        infoString.Add(healthConditions);
        infoString.Add(medication);
        infoString.Add(allergies);
        infoString.Add(membershipExpiration.ToShortDateString());

        Program.DB.Insert(tableName,
            "FirstName, LastName, TaekwondoRank, JudoRank, CourseType, CurrentStudent, JoinDate, School, Birthday, Address, PickupOne, RelationOne, CellOne, PickupTwo, RelationTwo, CellTwo, PickupThree, RelationThree, CellThree, PickupFour, RelationFour, CellFour, PhysicianName, PhysicianNumber, PhysicianExt, HospitalPref, HospitalPhone, HospitalExt, HealthConditions, Medication, Allergies, MembershipExpiration",
            string.Join(", ", infoString));
    }

    public string FirstName
    {
        get => firstName;
        set
        {
            firstName = value;
            Program.DB.Modify(id, tableName, "FirstName", firstName);
        }
    }

    public int Id => id;

    public DateTime JoinDate => joinDate;

    public string LastName
    {
        get => lastName;
        set
        {
            lastName = value;
            Program.DB.Modify(id, tableName, "LastName", lastName);
        }
    }

    public TaekwondoRank TaekwondoRank
    {
        get => taekwondoRank;
        set
        {
            taekwondoRank = value;
            Program.DB.Modify(id, tableName, "TaekwondoRank", Util.GetRankString(taekwondoRank));
        }
    }

    public JudoRank JudoRank
    {
        get => judoRank;
        set
        {
            judoRank = value;
            Program.DB.Modify(id, tableName, "JudoRank", Util.GetRankString(judoRank));
        }
    }

    public string CourseType
    {
        get => courseType;
        set
        {
            courseType = value;
            Program.DB.Modify(id, tableName, "CourseType", courseType);
        }
    }

    public bool CurrentStudent
    {
        get => currentStudent;
        set
        {
            currentStudent = value;
            Program.DB.Modify(id, tableName, "CurrentStudent", currentStudent.ToString());
        }
    }

    public string School
    {
        get => school;
        set
        {
            school = value;
            Program.DB.Modify(id, tableName, "School", school);
        }
    }

    public DateTime Birthday
    {
        get => birthday;
        set
        {
            birthday = value;
            Program.DB.Modify(id, tableName, "Birthday", birthday.ToShortDateString());
        }
    }

    public string Address
    {
        get => address;
        set
        {
            address = value;
            Program.DB.Modify(id, tableName, "Address", address);
        }
    }

    public string PickupOne
    {
        get => pickupOne;
        set
        {
            pickupOne = value;
            Program.DB.Modify(id, tableName, "PickupOne", pickupOne);
        }
    }

    public string RelationOne
    {
        get => relationOne;
        set
        {
            relationOne = value;
            Program.DB.Modify(id, tableName, "RelationOne", relationOne);
        }
    }

    public string CellOne
    {
        get => cellOne;
        set
        {
            cellOne = value;
            Program.DB.Modify(id, tableName, "CellOne", CellOne);
        }
    }

    public string PickupTwo
    {
        get => pickupTwo;
        set
        {
            pickupTwo = value;
            Program.DB.Modify(id, tableName, "PickupTwo", pickupTwo);
        }
    }

    public string RelationTwo
    {
        get => relationTwo;
        set
        {
            relationTwo = value;
            Program.DB.Modify(id, tableName, "RelationTwo", relationTwo);
        }
    }

    public string CellTwo
    {
        get => cellTwo;
        set
        {
            cellTwo = value;
            Program.DB.Modify(id, tableName, "cellTwo", cellTwo);
        }
    }

    public string PickupThree
    {
        get => pickupThree;
        set
        {
            pickupThree = value;
            Program.DB.Modify(id, tableName, "PickupThree", pickupThree);
        }
    }

    public string RelationThree
    {
        get => relationThree;
        set
        {
            RelationThree = value;
            Program.DB.Modify(id, tableName, "RelationThree", RelationThree);
        }
    }

    public string CellThree
    {
        get => cellThree;
        set
        {
            cellThree = value;
            Program.DB.Modify(id, tableName, "CellThree", cellThree);
        }
    }

    public string PickupFour
    {
        get => pickupFour;
        set
        {
            pickupFour = value;
            Program.DB.Modify(id, tableName, "PickupFour", pickupFour);
        }
    }

    public string RelationFour
    {
        get => relationFour;
        set
        {
            relationFour = value;
            Program.DB.Modify(id, tableName, "RelationFour", relationFour);
        }
    }

    public string CellFour
    {
        get => cellFour;
        set
        {
            cellFour = value;
            Program.DB.Modify(id, tableName, "CellFour", cellFour);
        }
    }

    public string PhysicianName
    {
        get => physicianName;
        set
        {
            physicianName = value;
            Program.DB.Modify(id, tableName, "PhysicianName", physicianName);
        }
    }

    public string PhysicianNumber
    {
        get => physicianNumber;
        set
        {
            physicianNumber = value;
            Program.DB.Modify(id, tableName, "PhysicianNumber", physicianNumber);
        }
    }

    public string PhysicianExt
    {
        get => physicianExt;
        set
        {
            physicianExt = value;
            Program.DB.Modify(id, tableName, "PhysicianExt", physicianExt);
        }
    }

    public string HospitalPref
    {
        get => hospitalPref;
        set
        {
            hospitalPref = value;
            Program.DB.Modify(id, tableName, "HospitalPref", hospitalPref);
        }
    }

    public string HospitalPhone
    {
        get => hospitalPhone;
        set
        {
            hospitalPhone = value;
            Program.DB.Modify(id, tableName, "HospitalPhone", hospitalPhone);
        }
    }

    public string HospitalExt
    {
        get => hospitalExt;
        set
        {
            hospitalExt = value;
            Program.DB.Modify(id, tableName, "HospitalExt", hospitalExt);
        }
    }

    public string HealthConditions
    {
        get => healthConditions;
        set
        {
            healthConditions = value;
            Program.DB.Modify(id, tableName, "HealthConditions", healthConditions);
        }
    }

    public string Medication
    {
        get => medication;
        set
        {
            medication = value;
            Program.DB.Modify(id, tableName, "Medication", medication);
        }
    }

    public string Allergies
    {
        get => allergies;
        set
        {
            allergies = value;
            Program.DB.Modify(id, tableName, "Allergies", allergies);
        }
    }

    public DateTime MembershipExpiration
    {
        get => membershipExpiration;
        set
        {
            membershipExpiration = value;
            Program.DB.Modify(id, tableName, "MembershipExpiration", membershipExpiration.ToShortDateString());
        }
    }

    private int id;
    private string firstName;
    private string lastName;

    private TaekwondoRank taekwondoRank;
    private JudoRank judoRank;
    private string courseType;
    private bool currentStudent;
    private DateTime membershipExpiration;
    private DateTime joinDate;

    private string school;

    private DateTime birthday;

    private string address;

    private string pickupOne;
    private string relationOne;
    private string cellOne;

    private string pickupTwo;
    private string relationTwo;
    private string cellTwo;

    private string pickupThree;
    private string relationThree;
    private string cellThree;

    private string pickupFour;
    private string relationFour;
    private string cellFour;

    private string physicianName;
    private string physicianNumber;
    private string physicianExt;

    private string hospitalPref;
    private string hospitalPhone;
    private string hospitalExt;

    private string healthConditions;
    private string medication;
    private string allergies;
}