﻿using System;
using System.Collections;
using System.Linq;
using HMAFlex.Controllers;

namespace HMAFlex.Models;

public class AMPStaff
{
    private string tableName;
    
    private int id;
    private string firstName;
    private string lastName;
    
    private DateTime birthday;
    private string phoneNumber;
    private string email;

    private TaekwondoRank taekwondoRank;
    
    private bool monday;
    private bool tuesday;
    private bool wednesday;
    private bool thursday;
    private bool friday;
    
    public AMPStaff(int id, string firstName, string lastName, bool monday, bool tuesday, bool wednesday, bool thursday, bool friday, DateTime birthday, string phoneNumber, string email, TaekwondoRank taekwondoRank)
    {
        tableName = "AMPStaff";
        
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.taekwondoRank = taekwondoRank;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;

        if (this.id == 0)
        {
            ArrayList infoString = new ArrayList();
            infoString.Add(firstName);
            infoString.Add(lastName);
            infoString.Add(birthday.ToShortDateString());
            infoString.Add(phoneNumber);
            infoString.Add(email);
            infoString.Add(Util.GetRankString(taekwondoRank));
            infoString.Add(monday.ToString());
            infoString.Add(tuesday.ToString());
            infoString.Add(wednesday.ToString());
            infoString.Add(thursday.ToString());
            infoString.Add(friday.ToString());
        
            Program.DB.Insert(tableName,
                "FirstName, LastName, Birthday, PhoneNumber, Email, TaekwondoRank, Monday, Tuesday, Wednesday, Thursday, Friday",
                string.Join(", ", infoString)); 
        }
    }

    public AMPStaff()
    {
        tableName = "AMPStaff";
        
        id = 0;
        firstName = "";
        lastName = "";
        birthday = DateTime.Now;
        phoneNumber = "";
        email = "";
        taekwondoRank = TaekwondoRank.None;
        monday = false;
        tuesday = false;
        wednesday = false;
        thursday = false;
        friday = false;

        ArrayList infoString = new ArrayList();
        infoString.Add(firstName);
        infoString.Add(lastName);
        infoString.Add(birthday.ToShortDateString());
        infoString.Add(phoneNumber);
        infoString.Add(email);
        infoString.Add(Util.GetRankString(taekwondoRank));
        infoString.Add(monday.ToString());
        infoString.Add(tuesday.ToString());
        infoString.Add(wednesday.ToString());
        infoString.Add(thursday.ToString());
        infoString.Add(friday.ToString());
        
        Program.DB.Insert(tableName,
            "FirstName, LastName, Birthday, PhoneNumber, Email, TaekwondoRank, Monday, Tuesday, Wednesday, Thursday, Friday",
            string.Join(", ", infoString));
    }

    public int Id => id;

    public string FirstName
    {
        get => firstName;
        set
        {
            firstName = value;
            Program.DB.Modify(id, tableName, "FirstName", firstName);
        }
    }

    public string LastName
    {
        get => lastName;
        set
        {
            lastName = value;
            Program.DB.Modify(id, tableName, "LastName", lastName);
        }
    }

    public DateTime Birthday
    {
        get => birthday;
        set 
        {
            birthday = value;
            Program.DB.Modify(id, tableName, "Birthday", birthday.ToShortDateString());
        }
    }

    public string PhoneNumber
    {
        get => phoneNumber;
        set 
        {
            phoneNumber = value;
            Program.DB.Modify(id, tableName, "PhoneNumber", phoneNumber);
        }
    }

    public string Email
    {
        get => email;
        set 
        {
            email = value;
            Program.DB.Modify(id, tableName, "Email", email);
        }
    }

    public TaekwondoRank TaekwondoRank
    {
        get => taekwondoRank;
        set 
        {
            taekwondoRank = value;
            Program.DB.Modify(id, tableName, "TaekwondoRank", Util.GetRankString(taekwondoRank));
        }
    }

    public bool Monday
    {
        get => monday;
        set 
        {
            monday = value;
            Program.DB.Modify(id, tableName, "Monday", monday.ToString());
        }
    }

    public bool Tuesday
    {
        get => tuesday;
        set 
        {
            tuesday = value;
            Program.DB.Modify(id, tableName, "Tuesday", tuesday.ToString());
        }
    }

    public bool Wednesday
    {
        get => wednesday;
        set 
        {
            wednesday = value;
            Program.DB.Modify(id, tableName, "Wednesday", wednesday.ToString());
        }
    }

    public bool Thursday
    {
        get => thursday;
        set 
        {
            thursday = value;
            Program.DB.Modify(id, tableName, "Thursday", thursday.ToString());
        }
    }

    public bool Friday
    {
        get => friday;
        set 
        {
            friday = value;
            Program.DB.Modify(id, tableName, "Friday", friday.ToString());
        }
    }
}