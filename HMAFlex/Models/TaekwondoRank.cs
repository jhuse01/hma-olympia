﻿namespace HMAFlex.Models;

public enum TaekwondoRank
{
    None,
    White,
    Yellow,
    Orange,
    Green,
    BlueTip,
    Blue,
    Purple,
    Brown,
    Red,
    BlackTip,
    Tiger,
    Dragon,
    TemporaryBlack,
    FirstDan,
    SecondDan,
    ThirdDan,
    FourthDan,
    FifthDan,
    SixthDan,
    SeventhDan
}