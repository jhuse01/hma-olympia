﻿namespace HMAFlex.Models;

public enum JudoRank
{
    None,
    White,
    Yellow,
    Orange,
    Green,
    Blue,
    Purple,
    Brown,
    Red,
    TemporaryBlack,
    FirstDan,
    SecondDan,
    ThirdDan,
    FourthDan,
    FifthDan,
    SixthDan
}