﻿namespace HMAFlex.Models;

public enum WebPermissionLevel
{
    Staff,
    School,
    HeadInstructor,
    Administrator,
}