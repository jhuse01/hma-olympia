﻿using System;
using System.Collections;
using System.Linq;
using HMAFlex.Controllers;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;

namespace HMAFlex.Models;

public class UserAction
{
    private static string tableName;
    
    private int id;
    private WebUser user;
    private string description;
    private DateTime performedOn;

    public UserAction()
    {
        tableName = "UserActions";
        
        id = 0;
        user = null;
        description = "";
        performedOn = DateTime.Now;

        ArrayList infoString = new ArrayList();
        infoString.Add(0); //WebUser ID 0
        infoString.Add(description);
        infoString.Add(performedOn.ToString());
        
        Program.DB.Insert(tableName,
            "UserID, Description, PerformedOn",
            string.Join(", ", infoString));
    }

    public UserAction(int id, WebUser user, string description, DateTime performedOn)
    {
        tableName = "UserActions";
        
        this.id = id;
        this.user = user;
        this.description = description;
        this.performedOn = performedOn;

        if (this.id == 0)
        {
            

            ArrayList infoString = new ArrayList();
            infoString.Add(0); //WebUser ID 0
            infoString.Add(description);
            infoString.Add(performedOn.ToString());
        
            Program.DB.Insert(tableName,
                "UserID, Description, PerformedOn",
                string.Join(", ", infoString));
        }
    }

    public WebUser User
    {
        get => user;
        set
        {
            user = value;
            Program.DB.Modify(id, tableName, "UserID", user.Id.ToString());
        }
    }

    public int Id => id;

    public string Description
    {
        get => description;
        set
        {
            description = value;
            Program.DB.Modify(id, tableName, "Description", description);
        }
    }

    public DateTime PerformedOn
    {
        get => performedOn;
        set
        {
            performedOn = value;
            Program.DB.Modify(id, tableName, "PerformedOn", performedOn.ToString());
        }
    }
}