﻿namespace HMAFlex.Models;

public enum CheckInOutType
{
    AMPStaff,
    AMPStudent,
    EveningClassStudent,
    SummerStaff,
    SummerStudent,
    None
}