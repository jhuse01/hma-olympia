﻿using System;
using System.Collections;
using System.Linq;
using HMAFlex.Controllers;

namespace HMAFlex.Models;

public class BirthdayParty
{
    private static string tableName;
    
    private int id;
    private DateTime bookedOn;
    
    private string firstNameOne;
    private string lastNameOne;
    private int ageOne;

    private string firstNameTwo;
    private string lastNameTwo;
    private int ageTwo;

    private string firstNameThree;
    private string lastNameThree;
    private int ageThree;

    private string partyDate;
    private string partyTime;

    private string estAttend;
    private string downPayment;
    private string phoneNumber;

    public BirthdayParty(int id, DateTime bookedOn, string firstNameOne, string lastNameOne, int ageOne, string firstNameTwo, string lastNameTwo, int ageTwo, string firstNameThree, string lastNameThree, int ageThree, string partyDate, string partyTime, string estAttend, string downPayment, string phoneNumber)
    {
        this.id = id;
        this.bookedOn = bookedOn;
        this.firstNameOne = firstNameOne;
        this.lastNameOne = lastNameOne;
        this.ageOne = ageOne;
        this.firstNameTwo = firstNameTwo;
        this.lastNameTwo = lastNameTwo;
        this.ageTwo = ageTwo;
        this.firstNameThree = firstNameThree;
        this.lastNameThree = lastNameThree;
        this.ageThree = ageThree;
        this.partyDate = partyDate;
        this.partyTime = partyTime;
        this.estAttend = estAttend;
        this.downPayment = downPayment;
        this.phoneNumber = phoneNumber;

        if (this.id == 0)
        {
            ArrayList infoString = new ArrayList();
            infoString.Add(bookedOn.ToShortDateString());
            infoString.Add(firstNameOne);
            infoString.Add(lastNameOne);
            infoString.Add(ageOne.ToString());
            infoString.Add(firstNameTwo);
            infoString.Add(lastNameTwo);
            infoString.Add(ageTwo.ToString());
            infoString.Add(firstNameThree);
            infoString.Add(lastNameThree);
            infoString.Add(ageThree.ToString());
            infoString.Add(partyDate);
            infoString.Add(partyTime);
            infoString.Add(estAttend);
            infoString.Add(downPayment);
            infoString.Add(phoneNumber);
        
            Program.DB.Insert(tableName,
                "BookedOn, FirstNameOne, LastNameOne, AgeOne, FirstNameTwo, LastNameTwo, AgeTwo, FirstNameThree, LastNameThree, AgeThree, PartyDate, PartyTime, EstAttend, DownPayment, PhoneNumber",
                string.Join(", ", infoString));
        }
    }

    public BirthdayParty()
    {
        id = 0;
        bookedOn = DateTime.Now;
        firstNameOne = "";
        lastNameOne = "";
        ageOne = 0;
        firstNameTwo = "";
        lastNameTwo = "";
        ageTwo = 0;
        firstNameThree = "";
        lastNameThree = "";
        ageThree = 0;
        partyDate = "";
        partyTime = "";
        estAttend = "";
        downPayment = "";
        phoneNumber = "";
        
        ArrayList infoString = new ArrayList();
        infoString.Add(bookedOn.ToShortDateString());
        infoString.Add(firstNameOne);
        infoString.Add(lastNameOne);
        infoString.Add(ageOne.ToString());
        infoString.Add(firstNameTwo);
        infoString.Add(lastNameTwo);
        infoString.Add(ageTwo.ToString());
        infoString.Add(firstNameThree);
        infoString.Add(lastNameThree);
        infoString.Add(ageThree.ToString());
        infoString.Add(partyDate);
        infoString.Add(partyTime);
        infoString.Add(estAttend);
        infoString.Add(downPayment);
        infoString.Add(phoneNumber);
        
        Program.DB.Insert(tableName,
            "BookedOn, FirstNameOne, LastNameOne, AgeOne, FirstNameTwo, LastNameTwo, AgeTwo, FirstNameThree, LastNameThree, AgeThree, PartyDate, PartyTime, EstAttend, DownPayment, PhoneNumber",
            string.Join(", ", infoString));
    }

    public int Id => id;

    public string FirstNameOne
    {
        get => firstNameOne;
        set => firstNameOne = value;
    }

    public string LastNameOne
    {
        get => lastNameOne;
        set => lastNameOne = value;
    }

    public int AgeOne
    {
        get => ageOne;
        set => ageOne = value;
    }

    public string FirstNameTwo
    {
        get => firstNameTwo;
        set => firstNameTwo = value;
    }

    public string LastNameTwo
    {
        get => lastNameTwo;
        set => lastNameTwo = value;
    }

    public int AgeTwo
    {
        get => ageTwo;
        set => ageTwo = value;
    }

    public string FirstNameThree
    {
        get => firstNameThree;
        set => firstNameThree = value;
    }

    public string LastNameThree
    {
        get => lastNameThree;
        set => lastNameThree = value;
    }

    public int AgeThree
    {
        get => ageThree;
        set => ageThree = value;
    }

    public string PartyDate
    {
        get => partyDate;
        set => partyDate = value;
    }

    public string PartyTime
    {
        get => partyTime;
        set => partyTime = value;
    }

    public string EstAttend
    {
        get => estAttend;
        set => estAttend = value;
    }

    public string DownPayment
    {
        get => downPayment;
        set => downPayment = value;
    }

    public string PhoneNumber
    {
        get => phoneNumber;
        set => phoneNumber = value;
    }

    public DateTime BookedOn => bookedOn;
}