﻿using System;
using HMAFlex.Controllers;

namespace HMAFlex.Models;

public class Configuration
{
    private DateTime nextTKDBeltTest;
    private DateTime nextJudoBeltTest;
    private string programName;
    private string tableName;

    public Configuration(DateTime nextTkdBeltTest, DateTime nextJudoBeltTest, string programName)
    {
        tableName = "Configuration";
        nextTKDBeltTest = nextTkdBeltTest;
        this.nextJudoBeltTest = nextJudoBeltTest;
        this.programName = programName;
    }

    public DateTime NextTKDBeltTest
    {
        get => nextTKDBeltTest;
        set
        {
            nextTKDBeltTest = value;
            Program.DB.Modify(1, tableName, "NextTKDBeltTest", nextTKDBeltTest.ToShortDateString());
        }
    }

    public DateTime NextJudoBeltTest
    {
        get => nextJudoBeltTest;
        set
        {
            nextJudoBeltTest = value;
            Program.DB.Modify(1, tableName, "NextJudoBeltTest", nextJudoBeltTest.ToShortDateString());
        }
    }

    public string ProgramName
    {
        get => programName;
        set
        {
            programName = value;
            Program.DB.Modify(1, tableName, "ProgramName", programName);
        }
    }
}