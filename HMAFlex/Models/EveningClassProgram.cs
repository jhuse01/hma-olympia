﻿namespace HMAFlex.Models;

public enum EveningClassProgram
{
    TrialLesson,
    OneYear,
    TwoYears,
    ThreeYears,
    MasterClub,
    Other
}