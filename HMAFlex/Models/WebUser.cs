﻿using System.Collections;
using System.Linq;
using HMAFlex.Controllers;

namespace HMAFlex.Models;

public class WebUser
{
    private static string tableName;
    
    private int id;
    private string firstName;
    private string lastName;
    private string username;
    private string email;
    private string password;
    private WebPermissionLevel permissionLevel;

    public WebUser()
    {
        tableName = "Users";

        id = 0;
        firstName = "";
        lastName = "";
        username = "";
        email = "";
        password = "";
        permissionLevel = WebPermissionLevel.Staff;
        
        ArrayList infoString = new ArrayList();
        infoString.Add(firstName);
        infoString.Add(lastName);
        infoString.Add(username);
        infoString.Add(email);
        infoString.Add(password);
        infoString.Add(Util.WebPermissionLevelToString(permissionLevel));

        Program.DB.Insert(tableName,
            "FirstName, LastName, Username, Email, Password, PermissionLevel",
            string.Join(", ", infoString));
    }

    public WebUser(int id, string firstName, string lastName, string username, string email, string password, WebPermissionLevel permissionLevel)
    {
        tableName = "Users";
        
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.permissionLevel = permissionLevel;

        if (id == 0)
        {
            ArrayList infoString = new ArrayList();
            infoString.Add(firstName);
            infoString.Add(lastName);
            infoString.Add(username);
            infoString.Add(email);
            infoString.Add(password);
            infoString.Add(Util.WebPermissionLevelToString(permissionLevel));

            Program.DB.Insert(tableName,
                "FirstName, LastName, Username, Email, Password, PermissionLevel",
                string.Join(", ", infoString));
        }
    }

    public string FirstName
    {
        get => firstName;
        set
        {
            firstName = value;
            Program.DB.Modify(id, tableName, "FirstName", firstName);
        }
    }

    public int Id => id;

    public string LastName
    {
        get => lastName;
        set
        {
            lastName = value;
            Program.DB.Modify(id, tableName, "LastName", lastName);
        }
    }

    public string Username
    {
        get => username;
        set
        {
            username = value;
            Program.DB.Modify(id, tableName, "Username", username);
        }
    }

    public string Email
    {
        get => email;
        set
        {
            email = value;
            Program.DB.Modify(id, tableName, "Email", email);
        }
    }

    public string Password
    {
        get => password;
        set
        {
            password = value;
            Program.DB.Modify(id, tableName, "Password", password);
        }
    }

    public WebPermissionLevel PermissionLevel
    {
        get => permissionLevel;
        set
        {
            permissionLevel = value;
            Program.DB.Modify(id, tableName, "PermissionLevel", Util.WebPermissionLevelToString(permissionLevel));
        }
    }
}