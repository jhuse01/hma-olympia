﻿using System;
using System.Collections.Generic;
using System.Linq;
using HMAFlex.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HMAFlex.Controllers
{
    public class AMPController : Controller
    {
        
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        
        public IActionResult PrintAttendance()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        
        public IActionResult EmergencyContact()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public IActionResult Staff()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        
        [HttpGet]
        public IActionResult CreateStudent()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        
        [HttpPost]
        public IActionResult CreateStudent(IFormCollection newStudent)
        {
            AMPStudent student = new AMPStudent();
            student = Program.AMPStudents.Last().Value;

            foreach (var (key, value) in newStudent)
            {
                if (key != "__RequestVerificationToken")
                {
                    switch (key)
                    {
                        case "FirstName":
                            student.FirstName = value;
                            break;
                        case "LastName":
                            student.LastName = value;
                            break;
                        case "PhysicianName":
                            student.PhysicianName = value;
                            break;
                        case "PhysicianExt":
                            student.PhysicianExt = value;
                            break;
                        case "PhysicianNumber":
                            student.PhysicianNumber = value;
                            break;
                        case "Birthday":
                            student.Birthday = DateTime.Parse(value);
                            break;
                        case "School":
                            student.School = value;
                            break;
                        case "HospitalPref":
                            student.HospitalPref = value;
                            break;
                        case "HospitalExt":
                            student.HospitalExt = value;
                            break;
                        case "HospitalPhone":
                            student.HospitalPhone = value;
                            break;
                        case "Address":
                            student.Address = value;
                            break;
                        case "HealthConditions":
                            student.HealthConditions = value;
                            break;
                        case "TaekwondoBeltRank":
                            student.TaekwondoRank = Util.GetTKDRank(value);
                            break;
                        case "JudoBeltRank":
                            student.JudoRank = Util.GetJudoRank(value);
                            break;
                        case "Medication":
                            student.Medication = value;
                            break;
                        case "Allergies":
                            student.Allergies = value;
                            break;
                        case "CourseType":
                            student.CourseType = value;
                            break;
                        case "CurrentStudent":
                            student.CurrentStudent = Boolean.Parse(value);
                            break;
                        case "MembershipExpiration":
                            student.MembershipExpiration = DateTime.Parse(value);
                            break;
                        case "PickupOne":
                            student.PickupOne = value;
                            break;
                        case "RelationOne":
                            student.RelationOne = value;
                            break;
                        case "CellOne":
                            student.CellOne = value;
                            break;
                        case "PickupTwo":
                            student.PickupTwo = value;
                            break;
                        case "RelationTwo":
                            student.RelationTwo = value;
                            break;
                        case "CellTwo":
                            student.CellTwo = value;
                            break;
                        case "PickupThree":
                            student.PickupThree = value;
                            break;
                        case "RelationThree":
                            student.RelationThree = value;
                            break;
                        case "CellThree":
                            student.CellThree = value;
                            break;
                        case "PickupFour":
                            student.PickupFour = value;
                            break;
                        case "RelationFour":
                            student.RelationFour = value;
                            break;
                        case "CellFour":
                            student.CellFour = value;
                            break;
                    }
                    
                    student = Program.AMPStudents.Last().Value;
                }
            }

            UserActionLog.AddEvent(Util.GetWebUser(Int32.Parse(HttpContext.Session.GetString("loggedInUser"))), "Create AMP Student - " + student.FirstName + " " + student.LastName + " (" + student.Id + ")");
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult EditStudent(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }

        [HttpPost]
        public IActionResult EditStudent(IFormCollection editForm)
        {
            int id = Int32.Parse(editForm["Id"]);
            AMPStudent student = Util.GetAMPStudent(id);
            
            foreach (var (key, value) in editForm)
            {
                if (key != "__RequestVerificationToken" || value != "")
                {
                    switch (key)
                    {
                        case "FirstName":
                            student.FirstName = value;
                            break;
                        case "LastName":
                            student.LastName = value;
                            break;
                        case "PhysicianName":
                            student.PhysicianName = value;
                            break;
                        case "PhysicianExt":
                            student.PhysicianExt = value;
                            break;
                        case "PhysicianNumber":
                            student.PhysicianNumber = value;
                            break;
                        case "Birthday":
                            student.Birthday = DateTime.Parse(value);
                            break;
                        case "School":
                            student.School = value;
                            break;
                        case "HospitalPref":
                            student.HospitalPref = value;
                            break;
                        case "HospitalExt":
                            student.HospitalExt = value;
                            break;
                        case "HospitalPhone":
                            student.HospitalPhone = value;
                            break;
                        case "Address":
                            student.Address = value;
                            break;
                        case "HealthConditions":
                            student.HealthConditions = value;
                            break;
                        case "TaekwondoBeltRank":
                            student.TaekwondoRank = Util.GetTKDRank(value);
                            break;
                        case "JudoBeltRank":
                            student.JudoRank = Util.GetJudoRank(value);
                            break;
                        case "Medication":
                            student.Medication = value;
                            break;
                        case "Allergies":
                            student.Allergies = value;
                            break;
                        case "CourseType":
                            student.CourseType = value;
                            break;
                        case "CurrentStudent":
                            student.CurrentStudent = Boolean.Parse(value);
                            break;
                        case "MembershipExpiration":
                            student.MembershipExpiration = DateTime.Parse(value);
                            break;
                        case "PickupOne":
                            student.PickupOne = value;
                            break;
                        case "RelationOne":
                            student.RelationOne = value;
                            break;
                        case "CellOne":
                            student.CellOne = value;
                            break;
                        case "PickupTwo":
                            student.PickupTwo = value;
                            break;
                        case "RelationTwo":
                            student.RelationTwo = value;
                            break;
                        case "CellTwo":
                            student.CellTwo = value;
                            break;
                        case "PickupThree":
                            student.PickupThree = value;
                            break;
                        case "RelationThree":
                            student.RelationThree = value;
                            break;
                        case "CellThree":
                            student.CellThree = value;
                            break;
                        case "PickupFour":
                            student.PickupFour = value;
                            break;
                        case "RelationFour":
                            student.RelationFour = value;
                            break;
                        case "CellFour":
                            student.CellFour = value;
                            break;
                    }
                    
                    student = Util.GetAMPStudent(id);
                }
            }
            
            UserActionLog.AddEvent(Util.GetWebUser(Int32.Parse(HttpContext.Session.GetString("loggedInUser"))), "Edit AMP Student - " + student.FirstName + " " + student.LastName + " (" + student.Id + ")");

            return RedirectToAction("Index");
        }
    
        [HttpGet]
        public IActionResult DeleteStudent(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;

            return View();
        }

        [HttpPost]
        public IActionResult DeleteStudent(IFormCollection form)
        {
            int id = Int32.Parse(form["Id"]);
            
            Program.DB.Remove(id, "AMPStudents");

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult CreateStaff()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public IActionResult CreateStaff(IFormCollection newStaff)
        {
            AMPStaff staff = new AMPStaff();
            staff = Program.AMPStaff.Last().Value;

            foreach (var (key, value) in newStaff)
            {
                if (key != "__RequestVerificationToken")
                {
                    switch (key)
                    {
                        case "FirstName":
                            staff.FirstName = value;
                            break;
                        case "LastName":
                            staff.LastName = value;
                            break;
                        case "Birthday":
                            staff.Birthday = DateTime.Parse(value);
                            break;
                        case "PhoneNumber":
                            staff.PhoneNumber = value;
                            break;
                        case "Monday":
                            staff.Monday = Boolean.Parse(value);
                            break;
                        case "Tuesday":
                            staff.Tuesday = Boolean.Parse(value);
                            break;
                        case "Wednesday":
                            staff.Wednesday = Boolean.Parse(value);
                            break;
                        case "Thursday":
                            staff.Thursday = Boolean.Parse(value);
                            break;
                        case "Friday":
                            staff.Friday = Boolean.Parse(value);
                            break;
                        case "Email":
                            staff.Email = value;
                            break;
                        case "TaekwondoBeltRank":
                            staff.TaekwondoRank = Util.GetTKDRank(value);
                            break;
                    }

                    staff = Program.AMPStaff.Last().Value;
                }
            }
            
            UserActionLog.AddEvent(Util.GetWebUser(Int32.Parse(HttpContext.Session.GetString("loggedInUser"))), "Create AMP Staff - " + staff.FirstName + " " + staff.LastName + " (" + staff.Id + ")");

            return RedirectToAction("Staff");
        }

        [HttpGet]
        public IActionResult EditStaff(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }

        [HttpPost]
        public IActionResult EditStaff(IFormCollection editForm)
        {
            int id = Int32.Parse(editForm["Id"]);
            AMPStaff staff = Util.GetAMPStaff(id);

            foreach (var (key, value) in editForm)
            {
                if (key != "__RequestVerificationToken")
                {
                    switch (key)
                    {
                        case "FirstName":
                            staff.FirstName = value;
                            break;
                        case "LastName":
                            staff.LastName = value;
                            break;
                        case "Birthday":
                            staff.Birthday = DateTime.Parse(value);
                            break;
                        case "PhoneNumber":
                            staff.PhoneNumber = value;
                            break;
                        case "Monday":
                            staff.Monday = Boolean.Parse(value);
                            break;
                        case "Tuesday":
                            staff.Tuesday = Boolean.Parse(value);
                            break;
                        case "Wednesday":
                            staff.Wednesday = Boolean.Parse(value);
                            break;
                        case "Thursday":
                            staff.Thursday = Boolean.Parse(value);
                            break;
                        case "Friday":
                            staff.Friday = Boolean.Parse(value);
                            break;
                        case "Email":
                            staff.Email = value;
                            break;
                        case "TaekwondoBeltRank":
                            staff.TaekwondoRank = Util.GetTKDRank(value);
                            break;
                    }
                }
            }
            
            return RedirectToAction("Staff");
        }

        [HttpGet]
        public IActionResult DeleteStaff(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }

        [HttpPost]
        public IActionResult DeleteStaff(IFormCollection form)
        {
            int id = Int32.Parse(form["Id"]);
            
            Program.DB.Remove(id, "AMPStaff");
            
            return RedirectToAction("Staff");
        }

        public IActionResult PrintCertificate(string type, int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;

            ViewBag.Type = type;
            
            UserActionLog.AddEvent(Util.GetWebUser(Int32.Parse(HttpContext.Session.GetString("loggedInUser"))), "Updated AMP Member Rank - (" + id + ", " + type + ").");

            return View();
        }

        public IActionResult DetailStaff(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }
        
        public IActionResult DetailStudent(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;

            return View();
        }
    }
}