using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HMAFlex.Controllers
{
    public class HtmlLayoutsController : Controller
    {
        public IActionResult AMPStudentEmergencyContact()
        {
            return View();
        }
        public IActionResult AMPAttendanceSheet()
        {
            return View();
        }
        public IActionResult ColorBeltTest()
        {
            return View();
        }
        public IActionResult SummerAttendanceSheet()
        {
            return View();
        }
        public IActionResult SummerStaffAttendanceSheet()
        {
            return View();
        }
        public IActionResult SummerStudentEmergencyContact()
        {
            return View();
        }
    }
}