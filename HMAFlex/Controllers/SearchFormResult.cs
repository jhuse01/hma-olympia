using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace HMAFlex.Controllers
{
    public static class SearchFormResult
    {
        public static Dictionary<string, string> GetSearchedUsers(string searchTerm)
        {
            Dictionary<string, string> results = new Dictionary<string, string>();

            XmlDocument ampStudents = new XmlDocument();
            ampStudents.Load("Views/AMP/AMPStudent.xml");
            XmlDocument eveningClassStudents = new XmlDocument();
            eveningClassStudents.Load("Views/EveningClass/EveningClassStudent.xml");
            XmlDocument summerStudents = new XmlDocument();
            summerStudents.Load("Views/Summer/SummerStudent.xml");
            
            XmlDocument ampStaff = new XmlDocument();
            ampStaff.Load("Views/AMP/AMPStaff.xml");
            XmlDocument summerStaff = new XmlDocument();
            summerStaff.Load("Views/Summer/SummerStaff.xml");

            XmlDocument userList = new XmlDocument();
            userList.Load("Views/Shared/UserInfo.xml");

            XmlDocument birthdayParties = new XmlDocument();
            birthdayParties.Load("Views/Birthdays/BirthdayParties.xml");

            foreach (XmlNode student in ampStudents.SelectNodes("AMP/Student"))
            {
                string name = student.Attributes["FirstName"].InnerText + " " + student.Attributes["LastName"].InnerText;
                if (name.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(student.Attributes["Id"].InnerText + " - AMP Students", name);
                }
            }
            
            foreach (XmlNode student in summerStudents.SelectNodes("SummerCamp/Student"))
            {
                string name = student.Attributes["FirstName"].InnerText + " " + student.Attributes["LastName"].InnerText;
                if (name.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(student.Attributes["Id"].InnerText + " - SummerCamp Students", name);
                }
            }
            
            foreach (XmlNode student in eveningClassStudents.SelectNodes("EveningClass/Student"))
            {
                string name = student.Attributes["FirstName"].InnerText + " " + student.Attributes["LastName"].InnerText;
                if (name.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(student.Attributes["Id"].InnerText + " - Evening Class Students", name);
                }
            }
            
            foreach (XmlNode staff in eveningClassStudents.SelectNodes("AMP/StaffMember"))
            {
                string name = staff.Attributes["FirstName"].InnerText + " " + staff.Attributes["LastName"].InnerText;
                if (name.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(staff.Attributes["Id"].InnerText + " - AMP Staff", name);
                }
            }
            
            foreach (XmlNode staff in eveningClassStudents.SelectNodes("SummerCamp/StaffMember"))
            {
                string name = staff.Attributes["FirstName"].InnerText + " " + staff.Attributes["LastName"].InnerText;
                if (name.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(staff.Attributes["Id"].InnerText +  " - SummerCamp Staff", name);
                }
            }

            foreach (XmlNode user in userList.SelectNodes("Users/User"))
            {
                string name = user.Attributes["firstName"].InnerText + " " + user.Attributes["lastName"].InnerText;
                if (name.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(user.Attributes["Id"].InnerText + " - Users", name);
                }
            }

            foreach (XmlNode party in birthdayParties.SelectNodes("HMAParties/BirthdayParty"))
            {
                string name1 = party.Attributes["FirstName1"].InnerText + " " + party.Attributes["LastName1"].InnerText;
                if (name1.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(party.Attributes["Id"].InnerText + "a - Birthday Parties", name1);
                }
                
                string name2 = party.Attributes["FirstName2"].InnerText + " " + party.Attributes["LastName2"].InnerText;
                if (name2.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(party.Attributes["Id"].InnerText + "b - Birthday Parties", name2);
                }
                
                string name3 = party.Attributes["FirstName3"].InnerText + " " + party.Attributes["LastName3"].InnerText;
                if (name3.ToUpper().Contains(searchTerm.ToUpper()))
                {
                    results.Add(party.Attributes["Id"].InnerText + "c - Birthday Parties", name3);
                }
            }

            return results;
        }
    }
}