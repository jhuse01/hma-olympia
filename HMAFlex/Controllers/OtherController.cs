using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using HMAFlex.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HMAFlex.Controllers
{
    public class OtherController : Controller
    {
        
        public IActionResult UserManagement()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public IActionResult ActionLog()
        {
            int loggedInID = Int32.Parse(HttpContext.Session.GetString("loggedInUser"));
            WebUser user = Util.GetWebUser(loggedInID);

            if (user.PermissionLevel != WebPermissionLevel.Administrator || user.PermissionLevel == WebPermissionLevel.HeadInstructor)
            {
                return RedirectToAction("Dashboard", "Home");
            }

            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpGet]
        public IActionResult CreateUser()
        {
            int loggedInID = Int32.Parse(HttpContext.Session.GetString("loggedInUser"));
            WebUser user = Util.GetWebUser(loggedInID);

            if (user.PermissionLevel != WebPermissionLevel.Administrator || user.PermissionLevel == WebPermissionLevel.HeadInstructor)
            {
                return RedirectToAction("Dashboard", "Home");
            }

            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        
        [HttpPost]
        public IActionResult CreateUser(IFormCollection newUser)
        {
            WebUser user = new WebUser();
            user = Program.WebUsers.Last().Value;
            
            foreach (var (key, value) in newUser)
            {
                if (key != "__RequestVerificationToken")
                {
                    switch (key)
                    {
                        case "firstName": user.FirstName = value; break;
                        case "lastName": user.LastName = value; break;
                        case "permissionLevel": user.PermissionLevel = Util.WebUserPermissionLevelFromString(value); break;
                        case "email": user.Email = value; break;
                        case "username": user.Username = value; break;
                        case "password": user.Password = value; break;
                    }

                    user = Program.WebUsers.Last().Value;
                }
            }

            return RedirectToAction("UserManagement");
        }

        [HttpGet]
        public IActionResult EditUser(int id)
        {
            int loggedInID = Int32.Parse(HttpContext.Session.GetString("loggedInUser"));
            WebUser user = Util.GetWebUser(loggedInID);

            if (user.PermissionLevel != WebPermissionLevel.Administrator || user.PermissionLevel == WebPermissionLevel.HeadInstructor)
            {
                return RedirectToAction("Dashboard", "Home");
            }

            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            return View();
        }

        public IActionResult EditUser(IFormCollection editForm)
        {
            int id = Int32.Parse(editForm["Id"]);
            WebUser user = Util.GetWebUser(id);

            foreach (var (key, value) in editForm)
            {
                if (key != "__RequestVerificationToken" && value != "")
                {
                    switch (key)
                    {
                        case "firstName": user.FirstName = value; break;
                        case "lastName": user.LastName = value; break;
                        case "permissionLevel": user.PermissionLevel = Util.WebUserPermissionLevelFromString(value); break;
                        case "email": user.Email = value; break;
                        case "username": user.Username = value; break;
                        case "password": user.Password = value; break;
                    }
                }
            }
            
            return RedirectToAction("UserManagement");
        }

        [HttpGet]
        public IActionResult DeleteUser(int id)
        {
            int loggedInID = Int32.Parse(HttpContext.Session.GetString("loggedInUser"));
            WebUser user = Util.GetWebUser(loggedInID);

            if (user.PermissionLevel != WebPermissionLevel.Administrator || user.PermissionLevel == WebPermissionLevel.HeadInstructor)
            {
                return RedirectToAction("Dashboard", "Home");
            }
            
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }

        [HttpPost]
        public IActionResult DeleteUser(IFormCollection form)
        {
            int id = Int32.Parse(form["Id"]);
            
            Program.DB.Remove(id, "Users");
            
            return RedirectToAction("UserManagement");
        }

        public IActionResult UserDetails(int id)
        {
            int loggedInID = Int32.Parse(HttpContext.Session.GetString("loggedInUser"));
            WebUser user = Util.GetWebUser(loggedInID);

            if (user.PermissionLevel != WebPermissionLevel.Administrator || user.PermissionLevel == WebPermissionLevel.HeadInstructor)
            {
                return RedirectToAction("Dashboard", "Home");
            }
            
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }

        [HttpGet]
        public IActionResult Configuration()
        {
            int loggedInID = Int32.Parse(HttpContext.Session.GetString("loggedInUser"));
            WebUser user = Util.GetWebUser(loggedInID);

            if (user.PermissionLevel != WebPermissionLevel.Administrator)
            {
                return RedirectToAction("Dashboard", "Home");
            }
            
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        
        [HttpPost]
        public IActionResult Configuration(IFormCollection editForm)
        {
            foreach (var (key, value) in editForm)
            {
                if (key != "__RequestVerificationToken")
                {
                    switch (key)
                    {
                        case "ProgramName": Program.Config.ProgramName = value; break;
                        case "TaekwondoTestingDate": Program.Config.NextJudoBeltTest = DateTime.Parse(value); break;
                        case "JudoTestingDate": Program.Config.NextTKDBeltTest = DateTime.Parse(value); break;
                    }
                }
            }

            return RedirectToAction("UserManagement");
        }
    }
}