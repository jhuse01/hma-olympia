﻿using System;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using HMAFlex.Models;
using Microsoft.AspNetCore.Http;

namespace HMAFlex.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(string username, string password)
        {
            string loggedIn = (HttpContext.Session.GetString("loggedIn"));

            if (loggedIn == "true")
            {
                Console.WriteLine("[ LOGIN ATTEMPT ] User Already Logged In: " +
                                  HttpContext.Session.GetString("loggedInUser"));
                return RedirectToAction("Dashboard");
            }
            
            foreach (WebUser user in Program.WebUsers.Values)
            {
                if (username == user.Username || username == user.Email)
                {
                    username = user.Username;
                    
                    Console.WriteLine("[ LOGIN ATTEMPT ] Matches user: " + username);

                    if (password != user.Password)
                    {
                        Console.WriteLine("[ LOGGIN FAIL ] Incorrect password for: " + username);
                    }
                    else
                    {
                        Console.WriteLine("[ LOGIN SUCCESS ] Logged in: " + username);
                        HttpContext.Session.SetString("loggedIn", "true");
                        ViewBag.userWelcomeMessage = user.FirstName + " " + user.LastName;
                        HttpContext.Session.SetString("loggedInUser", user.Id.ToString());
                        return RedirectToAction("Dashboard");
                    }
                }
            }

            HttpContext.Session.SetString("loggedIn", "false");
            return RedirectToAction("Index");
        }

        public IActionResult Dashboard()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index");
            }

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        public IActionResult LogOut()
        {
            HttpContext.Session.SetString("loggedIn", "false");
            HttpContext.Session.SetString("loggedInUser", "");
            
            return RedirectToAction("Index");
        }
    }
}
