using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Linq;
using System.Xml;
using HMAFlex.Models;

namespace HMAFlex.Controllers
{
    public class UserActionLog
    {
        public static void AddEvent(WebUser user, string eventDescription)
        {
            UserAction newEvent = new UserAction();
            newEvent = Program.UserActions.Last().Value;

            newEvent.Description = eventDescription;
            newEvent = Program.UserActions.Last().Value;

            newEvent.User = user;
        }
    }
}