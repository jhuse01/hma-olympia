﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using HMAFlex.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HMAFlex.Controllers
{
    public class EveningClassController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public IActionResult Expiration()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public IActionResult UpdateTrials()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public IActionResult UpcomingTrials()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public IActionResult Members()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public IActionResult TrialLessons()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [HttpGet]
        public IActionResult EditStudent(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Message = id;

            return View();
        }

        [HttpPost]
        public IActionResult EditStudent(IFormCollection editForm)
        {
            int id = Int32.Parse(editForm["Id"]);
            EveningClassStudent student = Util.GetEveningClassStudent(id);

            foreach (var (key, value) in editForm)
            {
                if (key != "__RequestVerificationToken" && value != "")
                {
                    switch (key)
                    {
                        case "FirstName":
                            student.FirstName = value;
                            break;
                        case "LastName":
                            student.LastName = value;
                            break;
                        case "CourseType":
                            student.CourseType = value;
                            break;
                        case "MembershipExpiration":
                            student.MembershipExpiration = DateTime.Parse(value);
                            break;
                        case "Birthday":
                            student.Birthday = DateTime.Parse(value);
                            break;
                        case "Email":
                            student.Email = value;
                            break;
                        case "TaekwondoBeltRank":
                            student.TaekwondoRank = Util.GetTKDRank(value);
                            break;
                        case "JudoBeltRank":
                            student.JudoRank = Util.GetJudoRank(value);
                            break;
                        case "Address":
                            student.Address = value;
                            break;
                        case "CurrentStudent":
                            student.CurrentStudent = Boolean.Parse(value);
                            break;
                        case "Status":
                            student.Status = Util.GetEveningProgram(value);
                            break;
                        case "ContactOne":
                            student.ContactOne = value;
                            break;
                        case "RelationOne":
                            student.RelationOne = value;
                            break;
                        case "PhoneOne":
                            student.PhoneOne = value;
                            break;
                        case "ContactTwo":
                            student.ContactTwo = value;
                            break;
                        case "RelationTwo":
                            student.RelationTwo = value;
                            break;
                        case "PhoneTwo":
                            student.PhoneTwo = value;
                            break;
                        case "ContactThree":
                            student.ContactThree = value;
                            break;
                        case "RelationThree":
                            student.RelationThree = value;
                            break;
                        case "PhoneThree":
                            student.PhoneThree = value;
                            break;
                    }

                    student = Util.GetEveningClassStudent(id);
                }
            }
            return RedirectToAction("Index");
        }
        

    [HttpGet]
        public IActionResult DeleteStudent(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            return View();
        }

        [HttpPost]
        public IActionResult DeleteStudent(IFormCollection form)
        {
            int id = Int32.Parse(form["Id"]);
            
            Program.DB.Remove(id, "EveningClassStudents");
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult CreateStudent()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public IActionResult CreateStudent(IFormCollection newStudent)
        {
            EveningClassStudent student = new EveningClassStudent();
            student = Program.EveningClassStudents.Last().Value;

            foreach (var (key, value) in newStudent)
            {
                if (key != "__RequestVerificationToken")
                {
                    switch (key)
                    {
                        case "FirstName":
                            student.FirstName = value;
                            break;
                        case "LastName":
                            student.LastName = value;
                            break;
                        case "CourseType":
                            student.CourseType = value;
                            break;
                        case "MembershipExpiration":
                            student.MembershipExpiration = DateTime.Parse(value);
                            break;
                        case "Birthday":
                            student.Birthday = DateTime.Parse(value);
                            break;
                        case "Email":
                            student.Email = value;
                            break;
                        case "TaekwondoBeltRank":
                            student.TaekwondoRank = Util.GetTKDRank(value);
                            break;
                        case "JudoBeltRank":
                            student.JudoRank = Util.GetJudoRank(value);
                            break;
                        case "Address":
                            student.Address = value;
                            break;
                        case "CurrentStudent":
                            student.CurrentStudent = Boolean.Parse(value);
                            break;
                        case "Status":
                            student.Status = Util.GetEveningProgram(value);
                            break;
                        case "ContactOne":
                            student.ContactOne = value;
                            break;
                        case "RelationOne":
                            student.RelationOne = value;
                            break;
                        case "PhoneOne":
                            student.PhoneOne = value;
                            break;
                        case "ContactTwo":
                            student.ContactTwo = value;
                            break;
                        case "RelationTwo":
                            student.RelationTwo = value;
                            break;
                        case "PhoneTwo":
                            student.PhoneTwo = value;
                            break;
                        case "ContactThree":
                            student.ContactThree = value;
                            break;
                        case "RelationThree":
                            student.RelationThree = value;
                            break;
                        case "PhoneThree":
                            student.PhoneThree = value;
                            break;
                    }

                    student = Program.EveningClassStudents.Last().Value;
                }
            }

            UserActionLog.AddEvent(Util.GetWebUser(Int32.Parse(HttpContext.Session.GetString("loggedInUser"))), "Create Evening Class Student - " + student.FirstName + " " + student.LastName + " (" + student.Id + ")");

            return RedirectToAction("Index");
        }

        public IActionResult PrintCertificate(string type, int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;

            ViewBag.Type = type;
            
            UserActionLog.AddEvent(Util.GetWebUser(Int32.Parse(HttpContext.Session.GetString("loggedInUser"))), "Updated Evening Class Member Rank - (" + id + ", " + type + ").");

            return View();
        }

        public IActionResult DetailStudent(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;

            return View();
        }
    }
}