using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using HMAFlex.Models;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Net.Http.Headers;
using Index = System.Index;

namespace HMAFlex.Controllers
{
    public class Util
    {
        private static int _testerId = 0;
        private static string _testerName = "";
        private static string _testerDate = "";
        private static string _testerTestType = "";
        private static string _testerBeltRank = "";

        public static void UpdateBeltTestCertificate(int id, string name, string date, string testType, string beltRank)
        {
            _testerId = id;
            _testerName = name;
            _testerDate = date;
            _testerTestType = testType;
            _testerBeltRank = beltRank;
        }

        public static int GetTesterId()
        {
            return _testerId;
        }

        public static string GetTesterName()
        {
            return _testerName;
        }

        public static string GetTesterDate()
        {
            return _testerDate;
        }

        public static string GetTesterTestType()
        {
            return _testerTestType;
        }

        public static string GetTesterBeltRank()
        {
            return _testerBeltRank;
        }

        public static string GetRankString(Object belt)
        {
            if (belt.GetType() == typeof(TaekwondoRank))
            {
                switch (belt)
                {
                    case TaekwondoRank.White: return "White Belt";
                    case TaekwondoRank.Yellow: return "Yellow Belt";
                    case TaekwondoRank.Orange: return "Orange Belt";
                    case TaekwondoRank.Green: return "Green Belt";
                    case TaekwondoRank.BlueTip: return "Blue-Tip Belt";
                    case TaekwondoRank.Blue: return "Blue Belt";
                    case TaekwondoRank.Purple: return "Purple Belt";
                    case TaekwondoRank.Brown: return "Brown Belt";
                    case TaekwondoRank.Red: return "Red Belt";
                    case TaekwondoRank.BlackTip: return "Black-Tip Belt";
                    case TaekwondoRank.Tiger: return "Tiger Belt";
                    case TaekwondoRank.Dragon: return "Dragon Belt";
                    case TaekwondoRank.TemporaryBlack: return "Temporary Black Belt";
                    case TaekwondoRank.FirstDan: return "First Dan";
                    case TaekwondoRank.SecondDan: return "Second Dan";
                    case TaekwondoRank.ThirdDan: return "Third Dan";
                    case TaekwondoRank.FourthDan: return "Fourth Dan";
                    case TaekwondoRank.FifthDan: return "Fifth Dan";
                    case TaekwondoRank.SixthDan: return "Sixth Dan";
                    case TaekwondoRank.SeventhDan: return "Seventh Dan";
                    default: return "None";
                }
            }

            if (belt.GetType() == typeof(JudoRank))
            {
                switch (belt)
                {
                    case JudoRank.White: return "White Belt";
                    case JudoRank.Yellow: return "Yellow Belt";
                    case JudoRank.Orange: return "Orange Belt";
                    case JudoRank.Green: return "Green Belt";
                    case JudoRank.Blue: return "Blue Belt";
                    case JudoRank.Purple: return "Purple Belt";
                    case JudoRank.Brown: return "Brown Belt";
                    case JudoRank.Red: return "Red Belt";
                    case JudoRank.TemporaryBlack: return "Temporary Black Belt";
                    case JudoRank.FirstDan: return "First Dan";
                    case JudoRank.SecondDan: return "Second Dan";
                    case JudoRank.ThirdDan: return "Third Dan";
                    case JudoRank.FourthDan: return "Fourth Dan";
                    case JudoRank.FifthDan: return "Fifth Dan";
                    case JudoRank.SixthDan: return "Sixth Dan";
                    default: return "None";
                }
            }

            return "None";
        }

        public static TaekwondoRank GetTKDRank(string rank)
        {
            switch (rank)
            {
                case "White Belt": return TaekwondoRank.White;
                case "Yellow Belt": return TaekwondoRank.Yellow;
                case "Orange Belt": return TaekwondoRank.Orange;
                case "Green Belt": return TaekwondoRank.Green;
                case "Blue-Tip Belt": return TaekwondoRank.BlueTip;
                case "Blue Belt": return TaekwondoRank.Blue;
                case "Purple Belt": return TaekwondoRank.Purple;
                case "Brown Belt": return TaekwondoRank.Brown;
                case "Red Belt": return TaekwondoRank.Red;
                case "Black-Tip Belt": return TaekwondoRank.BlackTip;
                case "Tiger Belt": return TaekwondoRank.Tiger;
                case "Dragon Belt": return TaekwondoRank.Dragon;
                case "Temporary Black Belt": return TaekwondoRank.TemporaryBlack;
                case "First Dan": return TaekwondoRank.FirstDan;
                case "Second Dan": return TaekwondoRank.SecondDan;
                case "Third Dan": return TaekwondoRank.ThirdDan;
                case "Fourth Dan": return TaekwondoRank.FourthDan;
                case "Fifth Dan": return TaekwondoRank.FifthDan;
                case "Sixth Dan": return TaekwondoRank.SixthDan;
                case "Seventh Dan": return TaekwondoRank.SeventhDan;
            }

            return TaekwondoRank.None;
        }

        public static JudoRank GetJudoRank(string rank)
        {
            switch (rank)
            {
                case "White Belt": return JudoRank.White;
                case "Yellow Belt": return JudoRank.Yellow;
                case "Orange Belt": return JudoRank.Orange;
                case "Green Belt": return JudoRank.Green;
                case "Blue Belt": return JudoRank.Blue;
                case "Purple Belt": return JudoRank.Purple;
                case "Brown Belt": return JudoRank.Brown;
                case "Red Belt": return JudoRank.Red;
                case "Temporary Black Belt": return JudoRank.TemporaryBlack;
                case "First Dan": return JudoRank.FirstDan;
                case "Second Dan": return JudoRank.SecondDan;
                case "Third Dan": return JudoRank.ThirdDan;
                case "Fourth Dan": return JudoRank.FourthDan;
                case "Fifth Dan": return JudoRank.FifthDan;
                case "Sixth Dan": return JudoRank.SixthDan;
            }

            return JudoRank.None;
        }

        public static string GetEveningProgramString(EveningClassProgram program)
        {
            switch (program)
            {
                case EveningClassProgram.OneYear: return "One Year";
                case EveningClassProgram.TwoYears: return "Two Years";
                case EveningClassProgram.ThreeYears: return "Black Belt Club";
                case EveningClassProgram.MasterClub: return "Master Club";
                case EveningClassProgram.TrialLesson: return "Trial Lesson";
                case EveningClassProgram.Other: return "Other";
            }

            return "Other";
        }

        public static EveningClassProgram GetEveningProgram(string program)
        {
            switch (program)
            {
                case "One Year": return EveningClassProgram.OneYear;
                case "Two Years": return EveningClassProgram.TwoYears;
                case "Black Belt Club": return EveningClassProgram.ThreeYears;
                case "Master Club": return EveningClassProgram.MasterClub;
                case "Trial Lesson": return EveningClassProgram.TrialLesson;
                case "Other": return EveningClassProgram.Other;
            }

            return EveningClassProgram.Other;
        }

        public static WebUser FindUserFromUsername(string username)
        {
            foreach (var user in Program.WebUsers)
            {
                if (user.Value.Username.ToUpper() == username)
                {
                    return user.Value;
                }
            }

            return null;
        }

        public static WebUser FindUserFromID(int id)
        {
            foreach (var (key, user) in Program.WebUsers)
            {
                if (key == id)
                {
                    return user;
                }
            }

            return null;
        }

        public static string WebPermissionLevelToString(WebPermissionLevel level)
        {
            switch (level)
            {
                case WebPermissionLevel.Staff: return "Staff";
                case WebPermissionLevel.School: return "School";
                case WebPermissionLevel.HeadInstructor: return "Head Instructor";
                case WebPermissionLevel.Administrator: return "Administrator";
            }

            return "Staff";
        }

        public static WebPermissionLevel WebUserPermissionLevelFromString(string level)
        {
            switch (level)
            {
                case "Staff": return WebPermissionLevel.Staff;
                case "School": return WebPermissionLevel.School;
                case "Head Instructor": return WebPermissionLevel.HeadInstructor;
                case "Administrator": return WebPermissionLevel.Administrator;
            }

            return WebPermissionLevel.Staff;
        }

        public static string CheckInOutTypeToString(CheckInOutType type)
        {
            switch (type)
            {
                case CheckInOutType.AMPStaff: return "AMPStaff";
                case CheckInOutType.AMPStudent: return "AMPStudent";
                case CheckInOutType.SummerStaff: return "SummerStaff";
                case CheckInOutType.SummerStudent: return "SummerStudent";
                case CheckInOutType.EveningClassStudent: return "EveningClassStudent";
            }

            return "None";
        }

        public static CheckInOutType CheckInOutTypeFromString(string type)
        {
            switch (type)
            {
                case "AMPStaff": return CheckInOutType.AMPStaff;
                case "AMPStudent": return CheckInOutType.AMPStudent;
                case "SummerStaff": return CheckInOutType.SummerStaff;
                case "SummerStudent": return CheckInOutType.SummerStudent;
                case "EveningClassStudent": return CheckInOutType.EveningClassStudent;
            }

            return CheckInOutType.None;
        }

        public static AMPStudent GetAMPStudent(int id)
        {
            foreach (var (key, ampStudent) in Program.AMPStudents)
            {
                if (key == id)
                {
                    return ampStudent;
                }
            }

            return null;
        }

        public static AMPStaff GetAMPStaff(int id)
        {
            foreach (var (key, ampStaff) in Program.AMPStaff)
            {
                if (key == id)
                {
                    return ampStaff;
                }
            }

            return null;
        }

        public static SummerStudent GetSummerStudent(int id)
        {
            foreach (var (key, summerStudent) in Program.SummerStudents)
            {
                if (key == id)
                {
                    return summerStudent;
                }
            }

            return null;
        }

        public static SummerStaff GetSummerStaff(int id)
        {
            foreach (var (key, summerStaff) in Program.SummerStaff)
            {
                if (key == id)
                {
                    return summerStaff;
                }
            }

            return null;
        }

        public static EveningClassStudent GetEveningClassStudent(int id)
        {
            foreach (var (key, eveningClassStudent) in Program.EveningClassStudents)
            {
                if (key == id)
                {
                    return eveningClassStudent;
                }
            }

            return null;
        }

        public static BirthdayParty GetBirthdayParty(int id)
        {
            foreach (var (key, birthdayParty) in Program.BirthdayParties)
            {
                if (key == id)
                {
                    return birthdayParty;
                }
            }

            return null;
        }

        public static CheckIn GetCheckIn(int id)
        {
            foreach (var (key, checkIn) in Program.CheckIns)
            {
                if (key == id)
                {
                    return checkIn;
                }
            }

            return null;
        }

        public static CheckOut GetCheckOut(int id)
        {
            foreach (var (key, checkOut) in Program.CheckOuts)
            {
                if (key == id)
                {
                    return checkOut;
                }
            }

            return null;
        }

        public static Configuration GetConfiguration()
        {
            return Program.Config;
        }

        public static UserAction GetUserAction(int id)
        {
            foreach (var (key, userAction) in Program.UserActions)
            {
                if (key == id)
                {
                    return userAction;
                }
            }

            return null;
        }

        public static WebUser GetWebUser(int id)
        {
            foreach (var (key, webUser) in Program.WebUsers)
            {
                if (key == id)
                {
                    return webUser;
                }
            }

            return null;
        }
    }
}