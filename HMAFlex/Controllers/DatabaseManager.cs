﻿using System;
using System.Collections;
using System.Collections.Generic;
using HMAFlex.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace HMAFlex.Controllers;

public class DatabaseManager
{
    public SqlConnectionStringBuilder cnnBuilder = new SqlConnectionStringBuilder();

    private SqlConnection cnn;

    public DatabaseManager()
    {
        cnnBuilder.DataSource = "tcp:hwangsmartialarts.database.windows.net,1433"; 
        cnnBuilder.UserID = "jhuse01";            
        cnnBuilder.Password = "Sabumnim 2017";     
        cnnBuilder.InitialCatalog = "HMA Flex";
        cnnBuilder.MultipleActiveResultSets = true;
        
        OpenConnection();
    }

    public void OpenConnection()
    {
        try
        {
            cnn = new SqlConnection(cnnBuilder.ToString());
            Connection().Open();
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine("Database Connected!");
            Console.WriteLine("---------------------------------------------");
        }
        catch (Exception e)
        {
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("");
            Console.WriteLine("Could not connect to the database at...");
            Console.WriteLine(cnnBuilder.ToString());
            Console.WriteLine("");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine(e.StackTrace);
        }
    }

    public void CloseConnection()
    {
        Connection().Close();
    }

    public void Insert(string tableName, string insertColumns, string insertValues)
    {
        string query = "INSERT INTO " + tableName + "(" + insertColumns + ") VALUES (" + insertValues + ")";

        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.InsertCommand = new SqlCommand(query, Connection());
        adapter.InsertCommand.ExecuteNonQuery();
        
        adapter.InsertCommand.Dispose();
        
        Program.UpdateActiveData();
    }

    public void Modify(int id, string tableName, string updateColumn, string updateValue)
    {
        string query = "UPDATE " + tableName + " SET " + updateColumn + "='" + updateValue + "' WHERE ID=" + id;

        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.UpdateCommand = new SqlCommand(query, Connection());
        adapter.UpdateCommand.ExecuteNonQuery();
        
        adapter.UpdateCommand.Dispose();

        Program.UpdateActiveData();
    }

    public void Remove(int id, string tableName)
    {
        string query = "DELETE " + tableName + " WHERE ID=" + id;

        SqlDataAdapter adapter = new SqlDataAdapter();
        adapter.DeleteCommand = new SqlCommand(query, Connection());
        adapter.DeleteCommand.ExecuteNonQuery();
        
        adapter.DeleteCommand.Dispose();
        
        Program.UpdateActiveData();
    }
    public SqlDataReader GetAll(string tableName, string column)
    {
        string query = "SELECT " + column + " FROM " + tableName;

        SqlCommand cmd = new SqlCommand(query, Connection());
        SqlDataReader dataReader = cmd.ExecuteReader();

        return dataReader;
    }
    
    public SqlConnection Connection()
    {
        return cnn;
    }
}