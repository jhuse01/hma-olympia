using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using HMAFlex.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HMAFlex.Controllers
{
    public class BirthdaysController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public IActionResult Calendar()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    
        [HttpGet]
        public IActionResult CreateParty()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        [HttpPost]
        public IActionResult CreateParty(IFormCollection newParty)
        {
            BirthdayParty party = new BirthdayParty();
            party = Program.BirthdayParties.Last().Value;

            foreach (var (key, value) in newParty)
            {
                if (key != "__RequestVerificationToken")
                {
                    switch (key)
                    {
                        case "FirstNameOne": party.FirstNameOne = value; break;
                        case "LastNameOne": party.LastNameOne = value; break;
                        case "AgeOne": party.AgeOne = Int32.Parse(value); break;
                        case "FirstNameTwo": party.FirstNameTwo = value; break;
                        case "LastNameTwo": party.LastNameTwo = value; break;
                        case "AgeTwo": party.AgeTwo = Int32.Parse(value); break;
                        case "FirstNameThree": party.FirstNameThree = value; break;
                        case "LastNameThree": party.LastNameThree = value; break;
                        case "AgeThree": party.AgeThree = Int32.Parse(value); break;
                        case "PartyDate": party.PartyDate = value; break;
                        case "PartyTime": party.PartyTime = value; break;
                        case "PhoneNumber": party.PhoneNumber = value; break;
                        case "EstimatedAttendees": party.EstAttend = value; break;
                        case "DownPayment": party.DownPayment = value; break;
                    }
                    party = Program.BirthdayParties.Last().Value;
                }
            }
            
            UserActionLog.AddEvent(Util.GetWebUser(Int32.Parse(HttpContext.Session.GetString("loggedInUser"))), "Create Birthday Party - " + party.FirstNameOne + " " + party.LastNameTwo + " (" + party.Id + ")");

            return RedirectToAction("Index");
        }
        
        [HttpGet]
        public IActionResult EditParty(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }

        [HttpPost]
        public IActionResult EditParty(IFormCollection editForm)
        {
            int id = Int32.Parse(editForm["Id"]);
            BirthdayParty party = Util.GetBirthdayParty(id);

            foreach (var (key, value) in editForm)
            {
                if (key != "__RequestVerificationToken" && value != "")
                {
                    switch (key)
                    {
                        case "FirstNameOne": party.FirstNameOne = value; break;
                        case "LastNameOne": party.LastNameOne = value; break;
                        case "AgeOne": party.AgeOne = Int32.Parse(value); break;
                        case "FirstNameTwo": party.FirstNameTwo = value; break;
                        case "LastNameTwo": party.LastNameTwo = value; break;
                        case "AgeTwo": party.AgeTwo = Int32.Parse(value); break;
                        case "FirstNameThree": party.FirstNameThree = value; break;
                        case "LastNameThree": party.LastNameThree = value; break;
                        case "AgeThree": party.AgeThree = Int32.Parse(value); break;
                        case "PartyDate": party.PartyDate = value; break;
                        case "PartyTime": party.PartyTime = value; break;
                        case "PhoneNumber": party.PhoneNumber = value; break;
                        case "EstimatedAttendees": party.EstAttend = value; break;
                        case "DownPayment": party.DownPayment = value; break;
                    }
                }
            }
            
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult DeleteParty(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }

        [HttpPost]
        public IActionResult DeleteParty(IFormCollection form)
        {
            int id = Int32.Parse(form["Id"]);
            
            Program.DB.Remove(id, "BirthdayParties");
            
            return RedirectToAction("Index");
        }

        public IActionResult PartyDetails(int id)
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Message = id;
            
            return View();
        }

        public IActionResult UpdateStaff()
        {
            if (HttpContext.Session.GetString("loggedIn") != "true")
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}