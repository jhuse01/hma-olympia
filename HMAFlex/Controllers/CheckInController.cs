using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HMAFlex.Controllers
{
    public class CheckInController : Controller
    {
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("loggedIn") != "false") {
                HttpContext.Session.SetString("loggedIn", "false");
                HttpContext.Session.SetString("loggedInUser", "");
            }

            int numAMPStudents = Program.AMPStudents.Count;
            int numAMPStaff = Program.AMPStaff.Count;
            int numSummerStudents = Program.SummerStudents.Count;
            int numSummerStaff = Program.SummerStaff.Count;
            int numEveningClass = Program.EveningClassStudents.Count;

            @ViewBag.numAMPStudent = numAMPStudents;
            @ViewBag.numAMPStaff = numAMPStaff;
            @ViewBag.numSummerStudent = numSummerStudents;
            @ViewBag.numSummerStaff = numSummerStaff;
            @ViewBag.numEveningClass = numEveningClass;
            
            return View();
        }
        
        public IActionResult SummerCamp()
        {
            if (HttpContext.Session.GetString("loggedIn") != "false") {
                HttpContext.Session.SetString("loggedIn", "false");
                HttpContext.Session.SetString("loggedInUser", "");
            }
            
            return View();
        }
        
        public IActionResult EveningClass()
        {
            if (HttpContext.Session.GetString("loggedIn") != "false") {
                HttpContext.Session.SetString("loggedIn", "false");
                HttpContext.Session.SetString("loggedInUser", "");
            }
            
            return View();
        }
        
        public IActionResult AMP()
        {
            if (HttpContext.Session.GetString("loggedIn") != "false") {
                HttpContext.Session.SetString("loggedIn", "false");
                HttpContext.Session.SetString("loggedInUser", "");
            }
            
            return View();
        }
    }
}