using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using HMAFlex.Controllers;
using HMAFlex.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace HMAFlex
{
    public class Program
    {
        private static DatabaseManager db;

        private static SortedDictionary<int, AMPStaff> ampstaff;
        private static SortedDictionary<int, AMPStudent> ampstudents;
        private static SortedDictionary<int, SummerStaff> summerstaff;
        private static SortedDictionary<int, SummerStudent> summerstudents;
        private static SortedDictionary<int, BirthdayParty> birthdayparties;
        private static SortedDictionary<int, EveningClassStudent> eveningclassstudents;
        private static SortedDictionary<int, CheckIn> checkins;
        private static SortedDictionary<int, CheckOut> checkouts;
        private static SortedDictionary<int, UserAction> useractions;
        private static SortedDictionary<int, WebUser> webusers;
        private static Configuration config;
        
        public static void Main(string[] args)
        {
            db = new DatabaseManager();
            
            UpdateActiveData();

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        public static void UpdateActiveData()
        {
            SqlDataReader reader;

            ampstaff = new SortedDictionary<int, AMPStaff>();
            reader = db.GetAll("AMPStaff", "*");

            while (reader.Read())
            {
                ampstaff.Add(
                    reader.GetInt32(0), 
                    new AMPStaff(
                        reader.GetInt32(0),
                        reader.GetString(1),
                        reader.GetString(2),
                        Boolean.Parse(reader.GetString(3)),
                        Boolean.Parse(reader.GetString(4)),
                        Boolean.Parse(reader.GetString(5)),
                        Boolean.Parse(reader.GetString(6)),
                        Boolean.Parse(reader.GetString(7)),
                        DateTime.Parse(reader.GetString(8)),
                        reader.GetString(9),
                        reader.GetString(10),
                        Util.GetTKDRank(reader.GetString(11))
                    ));
            }
            Console.WriteLine("Total AMP Staff: " + ampstaff.Count);
            
            reader.Close();
            ampstudents = new SortedDictionary<int, AMPStudent>();
            reader = db.GetAll("AMPStudents", "*");

            while (reader.Read())
            {
                ampstudents.Add(
                    reader.GetInt32(0),
                    new AMPStudent(
                        reader.GetInt32(0),
                        reader.GetString(1),
                        reader.GetString(2),
                        Util.GetTKDRank(reader.GetString(3)),
                        Util.GetJudoRank(reader.GetString(4)),
                        reader.GetString(5),
                        reader.GetString(6),
                        DateTime.Parse(reader.GetString(7)),
                        reader.GetString(8),
                        reader.GetString(9),
                        reader.GetString(10),
                        reader.GetString(11),
                        reader.GetString(12),
                        reader.GetString(13),
                        reader.GetString(14),
                        reader.GetString(15),
                        reader.GetString(16),
                        reader.GetString(17),
                        reader.GetString(18),
                        reader.GetString(19),
                        reader.GetString(20),
                        reader.GetString(21),
                        reader.GetString(22),
                        reader.GetString(23),
                        reader.GetString(24),
                        reader.GetString(25),
                        reader.GetString(26),
                        reader.GetString(27),
                        reader.GetString(28),
                        reader.GetString(29),
                        Boolean.Parse(reader.GetString(30)),
                        DateTime.Parse(reader.GetString(31)),
                        DateTime.Parse(reader.GetString(32))
                    )
                );
            }
            Console.WriteLine("Total Summer Staff: " + ampstudents.Count);

            reader.Close();
            summerstaff = new SortedDictionary<int, SummerStaff>();
            reader = db.GetAll("SummerStaff", "*");

            while (reader.Read())
            {
                summerstaff.Add(
                    reader.GetInt32(0), 
                    new SummerStaff(
                        reader.GetInt32(0),
                        reader.GetString(1),
                        reader.GetString(2),
                        Boolean.Parse(reader.GetString(3)),
                        Boolean.Parse(reader.GetString(4)),
                        Boolean.Parse(reader.GetString(5)),
                        Boolean.Parse(reader.GetString(6)),
                        Boolean.Parse(reader.GetString(7)),
                        DateTime.Parse(reader.GetString(8)),
                        reader.GetString(9),
                        reader.GetString(10),
                        Util.GetTKDRank(reader.GetString(11))
                    ));
            }
            Console.WriteLine("Total Summer Staff: " + summerstaff.Count);
            
            reader.Close();
            summerstudents = new SortedDictionary<int, SummerStudent>();
            reader = db.GetAll("SummerStudents", "*");

            while (reader.Read())
            {
                summerstudents.Add(
                    reader.GetInt32(0),
                    new SummerStudent(
                        reader.GetInt32(0),
                        reader.GetString(1),
                        reader.GetString(2),
                        Util.GetTKDRank(reader.GetString(3)),
                        Util.GetJudoRank(reader.GetString(4)),
                        reader.GetString(5),
                        reader.GetString(6),
                        DateTime.Parse(reader.GetString(7)),
                        reader.GetString(8),
                        reader.GetString(9),
                        reader.GetString(10),
                        reader.GetString(11),
                        reader.GetString(12),
                        reader.GetString(13),
                        reader.GetString(14),
                        reader.GetString(15),
                        reader.GetString(16),
                        reader.GetString(17),
                        reader.GetString(18),
                        reader.GetString(19),
                        reader.GetString(20),
                        reader.GetString(21),
                        reader.GetString(22),
                        reader.GetString(23),
                        reader.GetString(24),
                        reader.GetString(25),
                        reader.GetString(26),
                        reader.GetString(27),
                        reader.GetString(28),
                        reader.GetString(29),
                        Boolean.Parse(reader.GetString(30)), 
                        DateTime.Parse(reader.GetString(31)), 
                        DateTime.Parse(reader.GetString(32))
                    )
                );
            }
            Console.WriteLine("Total Summer Students: " + summerstudents.Count);
            
            reader.Close();
            birthdayparties = new SortedDictionary<int, BirthdayParty>();
            reader = db.GetAll("BirthdayParties", "*");

            while (reader.Read())
            {
                birthdayparties.Add(
                    reader.GetInt32(0),
                    new BirthdayParty(
                        reader.GetInt32(0),
                        DateTime.Parse(reader.GetString(1)),
                        reader.GetString(2),
                        reader.GetString(3),
                        Int32.Parse(reader.GetString(4)),
                        reader.GetString(5),
                        reader.GetString(6),
                        Int32.Parse(reader.GetString(7)),
                        reader.GetString(8),
                        reader.GetString(9),
                        Int32.Parse(reader.GetString(10)),
                        reader.GetString(11),
                        reader.GetString(12),
                        reader.GetString(13),
                        reader.GetString(14),
                        reader.GetString(15)
                    ));
            }
            Console.WriteLine("Total Birthday Parties: " + birthdayparties.Count);
            
            reader.Close();
            eveningclassstudents = new SortedDictionary<int, EveningClassStudent>();
            reader = db.GetAll("EveningClassStudents", "*");

            while (reader.Read())
            {
                eveningclassstudents.Add(
                    reader.GetInt32(0),
                    new EveningClassStudent(
                        reader.GetInt32(0),
                        reader.GetString(1),
                        reader.GetString(2),
                        reader.GetString(3),
                        Util.GetTKDRank(reader.GetString(4)),
                        Util.GetJudoRank(reader.GetString(5)),
                        Util.GetEveningProgram(reader.GetString(6)),
                        Boolean.Parse(reader.GetString(7)),
                        DateTime.Parse(reader.GetString(8)),
                        DateTime.Parse(reader.GetString(9)),
                        reader.GetString(10),
                        reader.GetString(11),
                        reader.GetString(12),
                        reader.GetString(13),
                        reader.GetString(14),
                        reader.GetString(15),
                        reader.GetString(16),
                        reader.GetString(17),
                        reader.GetString(18),
                        reader.GetString(19),
                        reader.GetString(20),
                        DateTime.Parse(reader.GetString(21))
                        )
                    );
            }
            Console.WriteLine("Total Evening Class Students: " + eveningclassstudents.Count);
            
            reader.Close();
            checkins = new SortedDictionary<int, CheckIn>();
            checkouts = new SortedDictionary<int, CheckOut>();
            reader = db.GetAll("CheckInOut", "*");

            while (reader.Read())
            {
                if (reader.GetString(5).ToUpper() == "IN")
                {
                    checkins.Add(
                        reader.GetInt32(0),
                        new CheckIn(
                            reader.GetInt32(0),
                            Util.CheckInOutTypeFromString(reader.GetString(1)),
                            Int32.Parse(reader.GetString(2)),
                            DateTime.Parse(reader.GetString(3)),
                            DateTime.Parse(reader.GetString(4))
                        )
                    );
                }
                else
                {
                    checkouts.Add(
                        reader.GetInt32(0),
                        new CheckOut(
                            reader.GetInt32(0),
                            Util.CheckInOutTypeFromString(reader.GetString(1)),
                            Int32.Parse(reader.GetString(2)),
                            DateTime.Parse(reader.GetString(3)),
                            DateTime.Parse(reader.GetString(4))
                        )
                    );
                }
            }

            Console.WriteLine("Total CheckIns: " + checkins.Count);
            Console.WriteLine("Total CheckOuts: " + checkouts.Count);

            reader.Close();
            webusers = new SortedDictionary<int, WebUser>();
            reader = db.GetAll("Users", "*");

            while (reader.Read())
            {
                webusers.Add(
                    reader.GetInt32(0),
                    new WebUser(
                        reader.GetInt32(0),
                        reader.GetString(1),
                        reader.GetString(2),
                        reader.GetString(3),
                        reader.GetString(4),
                        reader.GetString(5),
                        Util.WebUserPermissionLevelFromString(reader.GetString(6))
                    )
                    );
            }
            Console.WriteLine("Total Web Users: " + webusers.Count);
            
            reader.Close();
            useractions = new SortedDictionary<int, UserAction>();
            reader = db.GetAll("UserActions", "*");

            while (reader.Read())
            {
                useractions.Add(
                    reader.GetInt32(0),
                    new UserAction(
                        reader.GetInt32(0),
                        Util.FindUserFromID(Int32.Parse(reader.GetString(1))),
                        reader.GetString(2),
                        DateTime.Parse(reader.GetString(3))
                    )
                );
            }
            Console.WriteLine("Total User Actions: " + useractions.Count);

            reader.Close();
            reader = db.GetAll("Configuration", "*");
            while (reader.Read())
            {
                config = new Configuration(
                    DateTime.Parse(reader.GetString(1)),
                    DateTime.Parse(reader.GetString(2)),
                    reader.GetString(3)
                    );
            }
        }

        public static DatabaseManager DB => db;
        public static SortedDictionary<int, AMPStaff> AMPStaff => ampstaff;

        public static SortedDictionary<int, AMPStudent> AMPStudents => ampstudents;

        public static SortedDictionary<int, SummerStaff> SummerStaff => summerstaff;

        public static SortedDictionary<int, SummerStudent> SummerStudents => summerstudents;

        public static SortedDictionary<int, BirthdayParty> BirthdayParties => birthdayparties;

        public static SortedDictionary<int, EveningClassStudent> EveningClassStudents => eveningclassstudents;

        public static SortedDictionary<int, CheckIn> CheckIns => checkins;

        public static SortedDictionary<int, CheckOut> CheckOuts => checkouts;

        public static SortedDictionary<int, UserAction> UserActions => useractions;

        public static SortedDictionary<int, WebUser> WebUsers => webusers;

        public static Configuration Config => config;
    }
}
